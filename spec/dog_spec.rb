require 'spec_helper'
require_relative '../dog'

describe Dog do
  let(:dog){ described_class.new }

  it 'barks' do
    expect(dog.bark).to eq('Woof')
  end

  it 'jumps' do
    expect(dog.jump).to eq('Jump')
  end
end
